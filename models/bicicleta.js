var Bicicleta=function(id, color,modelo,ubicacion){
    this.id=id;
    this.color=color;
    this.modelo=modelo;
    this.ubicacion=ubicacion;
}

Bicicleta.prototype.toString=function(){
    return 'id '+this.id+" | color: ="+this.color;
}


Bicicleta.allBicis=[];

Bicicleta.add=function(aBici){
    this.allBicis.push(aBici);
}

Bicicleta.findById=function(aBiciId){
    var aBici=Bicicleta.allBicis.find(x=>x.id==aBiciId);
    if(aBici){
        console.log(`\n\t:->Se encontro el id ${aBici}`);
        console.log('\n');
        return aBici;
    }
    else{
        throw new Error(`No existe una bicicleta con id ${aBici}`); 
    }
          
}
Bicicleta.removeById=function(aBiciId){
    //no es necesario
    //Bicicleta.findById(aBiciId);
    for(var i=0;i<Bicicleta.allBicis.length;i++){
        if(Bicicleta.allBicis[i].id==aBiciId){
            Bicicleta.allBicis.splice(i,1); 
            //codigo adicional
         
            console.log(`\n\t:->Se borro el id ${aBiciId}`);
            console.log('\n');
            
            break;     
        }
    }
}

//los colores no se pueden repetir 
var a=new Bicicleta(1,'rojo','urbana',[9.040056313077315, -79.53380603405931]);
var b=new Bicicleta(2,'blanco','urbana',[9.039323361065048, -79.53295748772834]);
var c=new Bicicleta(3,'azul','urbana',[9.040157430215306, -79.53272570967906]);

Bicicleta.add(a);
Bicicleta.add(b);
Bicicleta.add(c);



//la exportacion al final siempre
module.exports=Bicicleta;
